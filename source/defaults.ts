namespace NS_Defaults{
	// maybe function so we can get our own colors
	// const nameColors = [
 //            ["Red", "#FF0000"],
 //            ["Blue", "#0000FF"],
 //            ["Green", "#00FF00"],
 //            ["FireBrick", "#B22222"],
 //            ["Coral", "#FF7F50"],
 //            ["YellowGreen", "#9ACD32"],
 //            ["OrangeRed", "#FF4500"],
 //            ["SeaGreen", "#2E8B57"],
 //            ["GoldenRod", "#DAA520"],
 //            ["Chocolate", "#D2691E"],
 //            ["CadetBlue", "#5F9EA0"],
 //            ["DodgerBlue", "#1E90FF"],
 //            ["HotPink", "#FF69B4"],
 //            ["BlueViolet", "#8A2BE2"],
 //            ["SpringGreen", "#00FF7F"]
 //            ];
		// blue
		// blue_violet
		// cadet_blue
		// chocolate
		// coral
		// dodger_blue
		// firebrick
		// golden_rod
		// green
		// hot_pink
		// orange_red
		// red
		// sea_green
		// spring_green
		// yellow_green
 //    export function get_color_for_user(name:string):string {
	//     var n = name.charCodeAt(0) + name.charCodeAt(name.length - 1);
	//     return nameColors[n % nameColors.length][1]
	// }
	const nameColors = [
		"#FF0000", 
		"#0000FF", 
		"#008000",
		"#B22222",
		"#FF7F50",
		"#9ACD32",
		"#FF4500",
		"#2E8B57",
		"#DAA520",
		"#D2691E",
		"#5F9EA0",
		"#1E90FF",
		"#FF69B4",
		"#8A2BE2",
		"#00FF7F"
	];
//hopefully better function
	export function get_color_for_user(name:string):string {
		var n = name.charCodeAt(0);
		return nameColors[n % nameColors.length]
	}


	export const getDefaultSettings = () => {return {
						messageStart: ":",
		        hideMessagesStartingWith: ["!"],
		        emoteThemeColor : "dark", 
		        message_color: false,
		        messageClasses: 'animateFromBottom color-light-grey font-larger',
		        showBadgesWhenMentioned : false, 
		        showUserColorWhenMentioned : true,
		        //set to false to remove whole message,so 'text' or false
		        //removed_message_message: 'message deleted', 
		        removed_message_message: false, 
		        removed_message_keep_name: false,
		        removeAfter: false,
			    	permission:{
			            "standard":{
			              //  "include":[],
			                "mask": null,
			                options:{
			                    "bold":true,
			                    "italic":true,
			                    "underline":true,
			                    "strikethrough":true,
			                    "monospace": true,
			                    "userColor": false,
			                    "msgColor": false,
			                    "fontSize": false,
			                    "msgStart": false
			                }
			            },
		            "subscriber":{
		               // "include":['standard'],
		                "mask": null,
		                options:{
			                	"bold":true,
				                "italic":true,
			                    "underline":true,
			                    "strikethrough":true,
			                    "monospace": true,
			                    "userColor": false,
			                    "msgColor": false,
			                    "fontSize": false,
			                    "msgStart": false
			                }

		            },
		            "vip":{
		               // "include":['subscriber'],
		                "mask": null,
		                options:{
			                	"bold":true,
				                "italic":true,
			                    "underline":true,
			                    "strikethrough":true,
			                    "monospace": true,
			                    "userColor": false,
			                    "msgColor": false,
			                    "fontSize": false,
			                    "msgStart": false
			                }
		                
		            },
		            "moderator":{
		                //"include":['vip'],
		                "mask": null,
		                options:{
			            	"bold":true,
				            "italic":true,
		                    "underline":true,
		                    "strikethrough":true,
		                    "monospace": true,
		                    "userColor": true,
		                    "msgColor": true,
		                    "fontSize": true,
		                    "msgStart": true
		                }
		                
		            },
		            "broadcaster":{
		                //"include":['moderator'],
		                "mask": null,
		                options:{
		                	"bold":true,
				            "italic":true,
		                    "underline":true,
		                    "strikethrough":true,
		                    "monospace": true,
		                    "userColor": true,
		                    "msgColor": true,
		                    "fontSize": true,
		                    "msgStart": true
		                }
		            }            
		        },
		        css: ""
		      }
		   }

} 