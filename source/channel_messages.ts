/// <reference path="user.ts" />
/// <reference path="chatmessage.ts" />
/// <reference path="logger.ts" />

namespace NS_ChannelMessages{
	//export class ChannelMessages{
		export let _messages: {[key:string]: NS_ChatMessage.ChatMessage} = {}
		export let _message_order : string[]= []
		export let _messages_by_user: {[key:number]:{[key:string]:NS_ChatMessage.ChatMessage}} = {}
		let _maxMsg = 64;

		export const add = (msg:NS_ChatMessage.ChatMessage):void => {
			_messages[msg.id] = msg;
			_message_order.push(msg.id);
			if(!_messages_by_user[msg.user.id]){
				_messages_by_user[msg.user.id]={};
			}
			_messages_by_user[msg.user.id][msg.id]= msg;
			_checkMaxMsg();
		}

		const _removeMsgRefsFromDicts = (msg:NS_ChatMessage.ChatMessage):void => {
			delete _messages[msg.id];
			delete _messages_by_user[msg.user.id][msg.id];
		}

		export const remove = (msg_id:string):void => _messages[msg_id]?.delete();


		export const _removeMsgRef = (msg: NS_ChatMessage.ChatMessage):void => { 
		    const index = _message_order.indexOf(msg.id)
		    if(index != -1){ // check if message is in the _message_order array
		      _message_order.splice(index, 1)
		    }
		    _removeMsgRefsFromDicts(msg)
  		}

  		const setMaxMsg = (n:number):void => {
  			_maxMsg=n;
  		}

		const _checkMaxMsg = ():void => {
			while(_message_order.length > _maxMsg ){
				Logger.info('maxMsg hit: remove oldest message')
				//@ts-ignore
				let msg = _messages[_message_order.shift()]
				msg.remove(false)
			    _removeMsgRefsFromDicts(msg)
			}
		}
	//}
}