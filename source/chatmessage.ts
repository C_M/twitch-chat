/// <reference path="logger.ts" />
/// <reference path="channels.ts" />
/// <reference path="user.ts" />
/// <reference path="cheernotes.ts" />



namespace NS_ChatMessage{

	interface emoteDict{
		'start': number;
		'end': number;
		'holder': string;
	}

	export class ChatMessage{
		_text;
		_tags;
		_channel: NS_Channels.Channel;
		_user: any = null//NS_User.User | undefined = undefined
		_emoteholders: any = []
		//_settings;
		
		constructor(channel:string, tags:any , text: any, self:any){
			Logger.info("channel")
			Logger.info(channel)

			this._text = text;
			this._tags = tags;
			this._channel = NS_Channels.channelsByID[parseInt(tags['room-id'])];
		}

		get channel(){ return this._channel;	}

		// todo: settings.settings can be better
		get settings(){	return this._channel.settings.settings; }

		get channelID(){ return this._tags['room-id']; }

		get user(){ return this._user; }

		get id():string{ return this._tags.id; }

		get type(){ return this._tags['message-type']; }

		get divOpen(){
			return `<div class="twi-message ${this.settings.messageClasses} user-group-${this.user.group}" id="${this.id}" roomid="${this.channel.id}">`
			//return `<div class="twi-message ${this.settings.messageClasses}" id="${this.id}" userid="${this.user.id}" roomid="${this.channel.id}">`
		}

		get userHTMLelement(){
			return `<p class='user_name ' style='color:${this.user.color}'>
				${this.user.badgesHTML()}${this.user.name}</p>`
		}

		parseCheermotes(text: string): string{
			console.log("check cheer")
			if(!this._tags.bits) return text; // not bits so return

			/* this doesn´t look efficient, but twitch has no clear rules for emote names
			so I guess this is one of the easier ways, thanks Twitch... 
			at least there must be a space for or after emote*/
			Logger.info("has bits: " + text)

			last_cheer = this // only for debug atm


			// lets use same system as emotes for safety, and to keep me sane
			let words = text.split(" ")
			const themeColor:"dark" | "light" = this.settings?.emoteThemeColor || 'dark' // lets assume dark theme as default
			const size:number = this.settings?.emoteSize || 1;
			for(let i = 0; i < words.length; ++i){
				console.log(words[i])
				const emote = NS_CheerEmotes.cheermotes[words[i]]
				if(emote){ // found cheermote, so replace
					console.log('found cheermote ' + words[i])
					const cheermoteholder = '<CM<'+(i)+'>>'
					const url = emote.images[themeColor].animated[size]
					words[i] = `<img src="${cheermoteholder}" />`
					this._emoteholders.push({'holder':cheermoteholder, 'url': url})
				}
			}
			return words.join(" ")
		}


		// function to handle / call functions to get the final text result
		get text_parsed(){
			let result = this.parseCheermotes(this.parse_emotes( this._text))
			result = this.parse_atUser(this.parse_formatting(result))
			if(this._tags['message-type'] === 'action'){
				return `<i>${result}</i></p>`}
			else{ 
				// return `${this.settings.messageStart}${result}</p>`
				return `${this.settings.messageStart}${result}</p>`
			}
		}

	// we don´t want to run unknown code, so sanatize the input
	//https://remarkablemark.org/blog/2019/11/29/javascript-sanitize-html/
		static sanitizeHTML (text: string): string{
			return $('<div>').text(text).html();
		}

		/* regex  from https://stackoverflow.com/a/70200736
			 *For bold text*
			 __For italic text__
			 ~For strikethrough text~
			 --For underlined text--
			 ```Monospace font``` */
		parse_formatting(s: string): string{ 
			const group = this.user.group
			const can = this._channel.settings.can
			console.log(this.settings)
			if (can(group, 'bold')){
				s = s.replace(/(?:\*)(?:(?!\s))((?:(?!\*|\n).)+)(?:\*)/g,'<b>$1</b>')
			}
			if (can(group, 'italic')){
				s = s.replace(/(?:__)(?:(?!\s))((?:(?!\n|__).)+)(?:__)/g,'<i>$1</i>')
			}
			if (can(group, 'strikethrough')){
				s = s.replace(/(?:~)(?:(?!\s))((?:(?!\n|~).)+)(?:~)/g,'<s>$1</s>')
			}
			if (can(group, 'underline')){
				s = s.replace(/(?:--)(?:(?!\s))((?:(?!\n|--).)+)(?:--)/g,'<u>$1</u>')
			}
			if (can(group, 'monospace')){
				s = s.replace(/(?:```)(?:(?!\s))((?:(?!\n|```).)+)(?:```)/g,'<tt>$1</tt>');
			}
			// todo: 
			for(let i = 0; i < this._emoteholders.length; ++i){
				s = s.replaceAll(this._emoteholders[i].holder, 
					this._emoteholders[i].url)
			}
			return s
		}

		get emotes():{[key:string]: string} {
			return this._tags.emotes;

		}


	// function to parse emotes
	// TODO: maybe switch to https://github.com/smilefx/tmi-emote-parse, gives bttv, ffz and 7tv emotes. However cheer emotes will likely still be problem
		parse_emotes(text: string): string{
			if(this._tags.emotes == null){
				Logger.info("NO  emotes")
				return ChatMessage.sanitizeHTML(text)      
			}
			Logger.info("parsing emotes")
			Logger.info(this._tags.emotes)
			let l = []
			let dict:{[key: number]: emoteDict} = {}
			let i = 0
			// get info for each emote
			for( const [key, val] of Object.entries(this.emotes) ){
				//var parts = emotes[i].split(":")
				// TODO: theme color
				let url = `https://static-cdn.jtvnw.net/emoticons/v2/${key}/default/dark/1.0`
				//let url = `https://static-cdn.jtvnw.net/emoticons/v1/${key}/1.0`
				// placeholder is because emotes url´s can have _ (underscore) in them,
				// which messes up de bold formatting, maybe use the emote text als holder, 
				// but this works for now
				let emoteholder = '<EM<'+(i++)+'>>'
				this._emoteholders.push({'holder':emoteholder, 'url': url})
				for(let j = 0; j < val.length; ++j){
					let temp = val[j].split("-")
					l.push(parseInt(temp[0]))
					dict[parseInt(temp[0])] = {'start': parseInt(temp[0]), 'end': parseInt(temp[1]), holder: emoteholder}
				}
			}
			l.sort((a,b) => b-a) // sorts with the numbers, because b-a it will reverse
			var result = ""
			for(  i = 0; i < l.length; ++i){
				result = `<img src="${dict[l[i]].holder}" />${ChatMessage.sanitizeHTML(text.slice(dict[l[i]].end+1))}${result}`
				text = text.slice(0, dict[l[i]].start)
			}
			text= `${ChatMessage.sanitizeHTML(text)}${result}`
			console.log(text)
			return text
		}

	// function to color @USER in the color for USER
		parse_atUser (text: string): string{
			//TODO::
			if(text.indexOf('@') == -1){ // no @ in msg, so return directly
				return text
			}
			let s = text.split('@')
			text = s[0]
			for(let i = 1; i < s.length; ++i){
				let s_splitted = s[i].split(" ")
				const u_name = s_splitted.shift()
				let u = undefined
				u = this.channel.getUserFromName( (u_name||"").toLowerCase())
				if(u){ // check if it is user otherwise just print as it was
					 text += `<span style='color:${(this.settings.showUserColorWhenMentioned ? u.color : "")}'>
					 @${(this.settings.showBadgesWhenMentioned ? u.badgesHTML() : "")}${u.name}
					 </span> ${(s_splitted.join(" "))}`
				}
				else{
					text += `@${s[i]}`
				}
			}
			return text
		}

	 // function to handle deleted messages through deletion from mod or ban / timeout
		delete(remove_refs = true){
			Logger.info("Message deleted by mod: "+this.id)
			if(this.settings.removed_message_message){
				$(`#${this.id} .message_text`).text('')
				$(`#${this.id} .message_text`).remove()
				if(!this.settings.removed_message_keep_name){
					$(`#${this.id} .user_name`).remove()
				}
				$('#'+this.id).append(`<p class='message_text deleted_message'>
					${this.settings.removed_message_message}</p>`)
			}
			else{
				$('#'+this.id).text("")
				this.remove(remove_refs)
			}     
		}

	// remove a message, optional remove also the references to the msg on other places
		remove(remove_refs = true){
			Logger.info(`remove this message ${this.id}`)
			$('#'+this.id).remove();
			if(remove_refs){
				this.channel.msg._removeMsgRef(this)
			}
		}

		doNotShow(): boolean{
			//TODO: check if user on ignore list
			//if there is an array, check for all
			if(Array.isArray(this.settings.hideMessagesStartingWith)){
				for(const item of this.settings.hideMessagesStartingWith){
					if(this._text.startsWith(item)) return true
				}
				return false
			}
			// ther is only one, check for that
			return this._text.startsWith(this.settings.hideMessagesStartingWith)
		}


	 //  function to create and show the html element for a message
		async process(){
			//check if on ignore list
			if(this.doNotShow()){
				Logger.info("not show msg " + this._text)
				return
			} 
			this._user = await this._channel.getUserFromTags(this._tags);
			this.channel.msg.add(this)

			last_message = this // only used for debug now for easy access in console
			let element = `${this.divOpen}${this.userHTMLelement}<p class='message_text'>
											${this.text_parsed}</div>`

			$('#chatwindow').append(element)
			const self = this // link to item itself, because we use inline function /other scoop
			if (this.settings.removeAfter){
				setTimeout(function(link:ChatMessage){
					Logger.info("remove: "+ self.id);
					$('#'+self.id).remove();}, this.settings.removeAfter*1000)
			}
		}
	}
	// last_message and last_cheer are only for debug too see what last message was
	export let last_message: ChatMessage | null = null;
	export let last_cheer: ChatMessage | null = null;

}