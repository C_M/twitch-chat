///<reference path="channels.ts" />
namespace NS_badges{
	// simple dict to store all the badges information in for the channels
	export let global:any | false=false;
	export let badges: {[key: number]:any} = {}

		export function getGlobalSets(){
			return global.badge_sets;
		}
		// load the global badges
		$.getJSON("https://badges.twitch.tv/v1/badges/global/display?language=en", function(data) {
				global = data;
		  });

		export function loadChannelBadges(channel:NS_Channels.Channel){
			const id= channel.id
			const url = `https://badges.twitch.tv/v1/badges/channels/${id}/display?language=en`;
			//const self = this // because of promise / async, copy this to use later
			$.getJSON(url, function(data) {	
			  badges[id] = data;
			  channel.badges = badges[id];
			  channel.ready = true;
			});
		}
}



