/// <reference path="logger.ts" />
/// <reference path="channel_settings.ts" />
/// <reference path="user.ts" />
/// <reference path="channelusers.ts" />
/// <reference path="channel_messages.ts" />
/// <reference path="database.ts" />
/// <reference path="badges.ts" />

namespace NS_Channels{
	export let joinList: string[] = [];
	export let channelsByName: {[key: string]: Channel} = {};
	export let channelsByID: {[key: number]: Channel} = {};

	{
		Logger.info("Loading channels to join from adres bar")
		let channelList = window.location.href.split("channel=")
		Logger.info("trying to get info")
		console.log(channelList)
		if(channelList.length > 1){
			let channel = channelList[1].split("&")[0]; // the split is to make sure only channel var is captured
			channelList = channel.split(',') // option to add more channels
			for(let i in channelList){
				joinList.push(channelList[i]);
			}
		}
	}

	export interface Roomstate{
		channel: string;
		'emote-only': boolean;
		'followers-only': string;
		'r9k': boolean;
		'room-id': string;
		slow:boolean;
		'subs-only': boolean;

	}


	export class Channel{
		_name: string;
		_id: number = -1;
		_ready = false;
		_badges = {}
		_users: NS_ChannelUsers.RoomUsers;
		_settings: NS_ChannelSettings.Settings;
		_roomstate: Roomstate | {} = {}

		_msg = NS_ChannelMessages; 

		constructor(name: string){
			Logger.info("printing channelsbyname")
			console.log(channelsByName)
			this._name = name;
			this._settings = new NS_ChannelSettings.Settings(this)
			channelsByName[name.toLowerCase()] = this;
			Logger.info("added channelsbyname")
			console.log(channelsByName)
			this._users = new NS_ChannelUsers.RoomUsers(this)


		}

		get msg(){
			return this._msg;
		}

		get settings(): NS_ChannelSettings.Settings{
			return this._settings;
		}

		get name():string{
			return this._name;
		}

		get id():number{
			return this._id;
		}

		get roomstate(): Roomstate | {}{
			return this._roomstate;
		}

		addUserToChannel(user: NS_User.User){
			this._users.addUserToChannel(user)
		}

		async updateRoomstate(tags: Roomstate){
			let id = parseInt(tags["room-id"])
			channelsByID[id] = this;
			this._id = id;
			if(!this._ready){ // badges not yet set, so get them
			  this.loadBadges()
			}
			if(!NS_Database.ready()){
				// later handle this, but should not happen, so request reload
				window.alert("DB not loaded, please reload page")
				return;
			}
			let r:any | undefined | null = await NS_Database.getChannelByIDPromise(this._id)
			if(r===undefined){ // channel not yet in database, store it
				Logger.info("new channel, add to database")
				this.saveChannel()
				return;
			} 
			console.log("result channel lookup");
			console.log(r)
			this._settings._settings = r.settings;


			//console.table(channelsByID)
			//this.saveChannel();
		}

		get badges(){
			return this._badges;
		}

		loadBadges():void{
			NS_badges.loadChannelBadges(this);
		}

		set badges(badges: any){
			this._badges = badges;
		}

		set ready(val: boolean){
			this._ready = val;
		}

		saveChannel():void{
			NS_Database.putChannel(this)
		}

		
		getUserFromTags(tags: {[key:string]: any}){
			return this._users.getUserFromTags(tags);
		}

		getUserFromName(name:string){
			console.log(['find on name', name])
			return this._users.getUserByName(name);
		}

	}

}