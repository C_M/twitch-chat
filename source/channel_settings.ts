/// <reference path="channels.ts" />
/// <reference path="defaults.ts" />
namespace NS_ChannelSettings{
		
	type viewerGroup = "standard" | "vip" | "moderator" | "subscriber" | "broadcaster";
	type viewerAction =  "bold" | "italic" | "underline" | "strikethrough" | "monospace" | 
	"userColor" | "msgColor" | "fontSize" | "msgStart"
		

	export class Settings{
		channel;
		_settings:{[key: string]: any} = {};

		get settings():{[key: string]: any}{
			return this._settings;

		}

		constructor(channel: NS_Channels.Channel){
			this.channel = channel;
			this._settings = NS_Defaults.getDefaultSettings()
		}


		can = (group:viewerGroup, action:viewerAction):boolean =>{
			//console.log([group, action, this._settings.permission[group]?.options[action]])

			return (this._settings.permission[group]?.options[action] || false)
		}

	}

}