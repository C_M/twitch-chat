/// <reference path="channels.ts" />
/// <reference path="database.ts" />
/// <reference path="defaults.ts" />
/// <reference path="badges.ts" />

namespace NS_User{

	export class User{
		_id: number=-1;
		_channel;
		_color:string | undefined;
		_group: "standard" | "moderator" | "vip" = "standard"
		_name: string = "";
		_badges_raw: {[key:string]:any} = {}
		_settings:{[key:string]:any} = {}
		_to_store = true;


		constructor(userID:number, channel: NS_Channels.Channel){
			this._id = userID;
			this._channel = channel;
			this._color = undefined; // todo: change to default color scheme
		}

		get settings():{[key:string]:any}{ return this._settings; }

		get color():string{
			return this._color || NS_Defaults.get_color_for_user(this.name)
		}

		get id(): number{ return this._id; }

		get name(): string{ return this._name;}

		get group(){ return this._group; }

		get channel(): NS_Channels.Channel{ return this._channel; }

		get badgesRaw():{[key:string]:any}{ return this._badges_raw; }

		update(tags: {[key: string]: any}){
			// for now, check everytime, if to big a strain, maybe do once several minutes
			let updated = false;

			if(this._name != tags['display-name']){
				this._name = tags['display-name'];
				updated = true;
			}

			// update if user has changed color -> wont work if user set custom color with command....
			if (this._color != tags.color){
				this._color = tags.color;
				if(this._color == ""){
					this._color = undefined;
				}
				updated = true;
			}
			// broadcast shoudn't be able to change so leave for now
			if(this._group == 'moderator' && tags.mod != 1){
			   this._group = 'standard';
			   updated = true;
			}else if(this._group == 'standard' && tags.mod === 1){
			   this._group = 'moderator';
			   updated = true;
			}


			if(this._badges_raw !== tags['badges-raw']){
				this._badges_raw = tags['badges-raw']
				updated = true;
			}
			if(updated){
				this._dataUpdated();
			}
			if(this._to_store){
				this._channel.addUserToChannel(this)
				this._to_store = false;
			}
			//NS_Database.getUserByName(this._name)
			return this;
		}

		_dataUpdated(){
			NS_Database.putUser(this);
			return;
		}

		async getDBinfo(){
			const r:any= await NS_Database.getUserByIDsPromise(this.channel.id, this.id)
			if(r){
				this._name = r.name;
				this._group = r.group;
				this._color = r.color;
				this._badges_raw = r._badges_raw;
				Logger.info("found user")
				console.log(r)
			}

		}

		get badges(){
			//return [];
			if( this._badges_raw == null){ // no badges so no need to try to find them
				return [];
			}
			const _badges = this._badges_raw.split(",") // get all badges in an array
			let result = []
			const ch = this.channel.badges.badge_sets
			const gl = NS_badges.getGlobalSets()
			for (var i = 0; i < _badges.length; ++i){
			  let b = _badges[i].split("/") // split badge name and version part of description
			  if(b[0] in ch && b[1] in ch[b[0]].versions){ // check if badge is defined in channel badge list
				result.push(ch[b[0]].versions[b[1]])
			  }
			  else if(b[0] in gl){ // otherwise get info from global badge list
				result.push(gl[b[0]].versions[b[1]])
			  }
			  //result += `<img src="${curr_badge.versions[b[1]].image_url_1x}">`
			}
			return result;
		}

		badgesHTML(size_url:string = 'image_url_1x'):string{
			let b = this.badges
			let result = ""
			for(let i = 0; i < b.length; ++i){
				result += `<img src="${b[i][size_url]}" class="badge badge_${b[i].title}">`
			}
			return result
		}


	}



}