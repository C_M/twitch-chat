/// <reference path="channels.ts" />
/// <reference path="database.ts" />
namespace NS_ChannelUsers{
	export class RoomUsers{
		_channel: NS_Channels.Channel;
		_users: {[key:number]: NS_User.User} = {}
		_usersByName: {[key:string]: NS_User.User} = {}


		constructor(channel: NS_Channels.Channel){
			this._channel=channel;
		}

		addUserToChannel(user: NS_User.User){
			this._users[user.id] = user;
			this._usersByName[user.name.toLowerCase()] = user;			
		}

		getUserByID(id: number): NS_User.User | undefined{
			return this._users[id];
		}

		getUserByName(name: string): NS_User.User | undefined{
			return this._usersByName[name];
		}

		async getUserFromTags(tags:{[key: string]: any}){
			let user = this.getUserByID(parseInt(tags['user-id']));
			if(!user){
				// user not in memory-> create user with userID
				user = new NS_User.User(parseInt(tags['user-id']), this._channel);
				Logger.info(" user not in memory, check db")
				await user.getDBinfo();
			}			 // check if user is in database
				

			// check if user must be updated with info from tags
			return user.update(tags)		
		}
	}
}
