/// <reference path="user.ts" />
/// <reference path="channels.ts" />


namespace NS_Database{
	let name = 'c_m_twitch_chat'
	let version = 2.0;
	//let db: {[key:string]:any} = {}

	// @ts-ignore
	let indexedDB = window.indexedDB

	let db:any = undefined;
	let _ready = false;

	export function ready(){
		return _ready;
	}

	export function putUser(user: NS_User.User){
		if(!db){
			console.log("err: db not opened succesfully")
		}
		const transaction = db.transaction("users", "readwrite");
		const users = transaction.objectStore("users");
		const roomIDIndex = users.index("roomID");
		const userIDindex = users.index("userID");
		const values = {roomID: user.channel.id , userID:user.id, 'name':user.name, 'nameLowercase':user.name.toLowerCase(), 'color': user.color,
			'group': user.group, 'badges_raw': user.badgesRaw} 
		users.put(values);
	}


	export function putChannel(channel: NS_Channels.Channel){
		if(!db){
			console.log("err: db not opened succesfully")
		}

		const transaction = db.transaction("channels", "readwrite");
		const channels = transaction.objectStore("channels");
		//const nameIndex = users.index()
		let values = {id: channel.id, name:channel.name.toLowerCase(), settings:channel.settings.settings}

		console.log("VALUESS CHANNEL")
		console.log(values)
		channels.put(values);
	}

	export function getChannelByIDPromise(id:number){
		return new Promise((resolve) => 
			{
				const channel = db.transaction("channels", 'readonly').objectStore("channels");
				let request = channel.get(id)

				request.onsuccess = function() {
					let res = request.result;
					resolve(res);
				};
				request.onerror = function() {
					let res = request.result;
					resolve(null);
				};
			}
		);			
	}

	export function getChannelByNamePromise(name:string){
				return new Promise((resolve) => 
			{
				const channel = db.transaction("channels", 'readonly').objectStore("channels");
				const name = channel.index("name")
				let request = name.get(name.toLowerCase())

				request.onsuccess = function() {
					let res = request.result;
					resolve(res);
				};
				request.onerror = function() {
					let res = request.result;
					resolve(null);
				};
			}
		);			
	}
		


	export function getUserByIDsPromise(roomID: number, userID:number){
		return new Promise((resolve) => 
			{
				const users = db.transaction("users", 'readonly').objectStore("users");
				let request = users.get([roomID, userID])

				request.onsuccess = function() {
					let res = request.result;
					resolve(res);
				};
				request.onerror = function() {
					let res = request.result;
					resolve(null);
				};
			}
		);			
	}

	export function getUserByName(name:string){
		const users = db.transaction("users", 'readonly').objectStore("users");
		const lc_index = users.index("nameLowercase")
		console.log(name)
		let request = lc_index.get(name.toLowerCase())
		  request.onsuccess = function () {
		  	console.log("get db succes")
		    console.log(request.result)
		    }
	}


	export function getUserByNamePromise(roomID:number, userID:number) {
		console.log("DBGET")
		return new Promise((resolve) => 
			{
				const users = db.transaction("users", 'readonly').objectStore("users");
				let request = users.get([roomID, userID])

				request.onsuccess = function() {
					let res = request.result;
					resolve(res);
				};
				request.onerror = function() {
					let res = request.result;
					resolve(null);
				};
			}
		);	
	}

	const request = indexedDB.open(name,version);

	request.onsuccess = function(){
		console.log("opened")
		db = request.result;
		_ready=true;

	}

	request.onerror = function (event) {
		  console.error("An error occurred with IndexedDB");
		  console.error(event);
		  window.alert("something went wrong with local database, please reload page")
		};

	request.onupgradeneeded = function (upgradeDB) {
		console.log("UPGRADE")
		  const db = request.result;
		  switch(upgradeDB.oldVersion){
		  	case 0: const users = 
			  	db.createObjectStore("users", { keyPath: ['roomID', 'userID'] });
			  	users.createIndex("roomID", "roomID", { unique: false });
			  	users.createIndex("userID", "userID", {unique:false});
			  	users.createIndex("nameLowercase", "nameLowercase", {unique:true});


				const channels = db.createObjectStore("channels", { keyPath: 'id' });
				channels.createIndex('name', 'name', {unique: true})
		  }		 
		};

}