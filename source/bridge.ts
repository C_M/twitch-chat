/// <reference path="channels.ts" />
/// <reference path="logger.ts" />

namespace bridge{
	interface tmi_bridge{
		connect: Function;
		on: Function;
		disconnect: Function;
		join: Function;
		part: Function;
		ws: any;

	}

	export class Bridge{
		_client: tmi_bridge | null;
		_logger: any;

		constructor(logger: any){
			this._client = null
			this.connect()
			this._logger = logger
		}

//return [ 'CONNECTING', 'OPEN', 'CLOSING', 'CLOSED' ][this.ws.readyState];
		connect(): void{
		  // @ts-ignore
			this._client = new tmi.Client({
				options: { debug: true, messagesLogLevel: "info" },
							connection: {
								reconnect: true,
								secure: true
							  }, 
							// identity: {
							//   username: 'bot-name',
							//   password: 'oauth:my-bot-token'
							// },
						  //  channels: [ 'my-channel' ]
							});
			if(this._client != null){
				this._addListeners();
				this._client.connect().catch(console.error);
			}
		}

		_addListeners(): void{
			if(this._client != null){
				this._client.on('connected', this._onServerConnected.bind(this))
				this._client.on('message', this._onMessage.bind(this) )
				this._client.on('cheer', this._onMessage.bind(this) )
				this._client.on('roomstate', this._onRoomstate.bind(this))
				//this._client.on('join', this._onJoin.bind(this))
				this._client.on('raw_message', this._onRawMessage.bind(this))
				this._client.on('messagedeleted', this._onMessageDeleted.bind(this))
				//this._client.on('ban', this._onBan.bind(this))
				//this._client.on('timeout', this._onTimeout.bind(this))
			}
		}

		connected(): boolean{ // same herè, maybe find function for ws
			return (this._client?.ws?.readyState == 1)
		}

		readyStateString(): string{
			let state: number = this._client?.ws.readyState || 4 // maybe find tmi.js function for it
			return `${state} (${[ 'CONNECTING', 'OPEN', 'CLOSING', 'CLOSED' ][state]})`
		}

		get log(){
			return this._logger
		}

		get client():tmi_bridge | null{
			return this._client
		}  

		_onRawMessage(msg:any, raw:any): void {
			if(raw.command == 'JOIN' || raw.command == 'PART'){
				return // not going to print alll joins an parts
			}
			console.log(raw)
		}

		_onMessage(channel:string, tags:{[key: string]: any}, text:string, self:any){
			console.log("DEBUG")
			console.log([channel,tags, text, self])
			const msg = new NS_ChatMessage.ChatMessage(channel, tags, text, self)
			this.log.info("message received on channel: " + channel)
			msg.process()
		}

		_onMessageDeleted(channel_name_with_pound:string, username:string, deletedMessage:string, tags:any){
			console.log([channel_name_with_pound, username, deletedMessage, tags])
			NS_Channels.channelsByName[channel_name_with_pound.slice(1)].msg.remove(tags['target-msg-id'])
			return
		}

		_onServerConnected(server: string, port: number): void {
						// TODO: added to channel so get the badges list (or notice that connected etc)
				// dunno if we can use the joined or we need to read rawmessage
			this.log.info(`connected to: ${server}:${port}`)
			this.log.info("Trying to join the channels on the list")
			console.log(NS_Channels.joinList)
			for(let i in NS_Channels.joinList){				
				this.join_channel(NS_Channels.joinList[i])
			}
		}

		join_channel(channel_name: string): void {
			/* when channel is left and rejoined, could reload badges and create new channel
			without that need for, but also could be seen as reset / reload */
			new NS_Channels.Channel(channel_name) 
			this._client?.join(channel_name)
			// dunno what happens if tmi is already listining to channel
			this.log.info(`joining ${channel_name}`)
		}

		_onRoomstate(channel_name_with_pound: string, tags:NS_Channels.Roomstate){
			Logger.info("joined channel: "+channel_name_with_pound)
			console.log(tags)
			const name = channel_name_with_pound.slice(1)  // slice 1 because of the # before the name		
			console.log(name)
			console.log(NS_Channels.channelsByName[name])
			NS_Channels.channelsByName[name].updateRoomstate(tags) 
		}


		leave_channel(channel_name: string): void{
			this._client?.part(channel_name)
			//maybe also remove from local channellist
			this.log.info(`leaving ${channel_name}`)
		}

		disconnect(): void{
			this._client?.disconnect()
		}



	}

}
