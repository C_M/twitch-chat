/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
namespace Logger{
	let level=4;
	// simple function to have a log window possible
	export function init(lvl=4): void{
		level = lvl;
	}

	function log(msg: string): void{
	  let date = new Date()
	  let s = `${String(date.getHours()).padStart(2, "0")}:${String(date.getMinutes()).padStart(2, "0")}:${String(date.getSeconds()).padStart(2, "0")} ${msg}`
	  $('#console').append(`<p>${s}</p>`)

	  console.log(s)
	  // remove everything except last 25 for performance reasons
	  $("#console").children().slice(0, -25).remove();

	 // $('#chatwindow').append(`<p>${msg}</p>`)
	}

	export function info(msg: string): void{
	    log(msg)
	}
	export function debug(msg: string): void{
	    log(msg)
	}
}